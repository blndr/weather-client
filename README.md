# Weather CLI Client

Write a command line tool that takes a city name as a parameter and displays if it’s going to rain today using MetaWeather (https://www.metaweather.com/api/).

## Relevant API Endpoints

- /api/location/search/?query=(query)
- /api/location/(woeid)/

## Usage

`python3 run.py '<city name>'`

## Run the tests
    virtualenv venv -p python3
    source venv/bin/activate
    pip install -r requirements.txt
    pytest test
    
### Get the test coverage

Just do the previous and run `pytest --cov=client test`