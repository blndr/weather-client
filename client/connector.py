import requests


def search_location(keyword: str) -> dict:
    response = requests.get('https://www.metaweather.com/api/location/search/?query=%s' % keyword)
    return response.json()


def fetch_forecast(woeid: int) -> dict:
    response = requests.get('https://www.metaweather.com/api/location/%s/' % woeid)
    return response.json()
