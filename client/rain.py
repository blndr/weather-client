from typing import Callable, List


class LocationSearchFailure(Exception):
    def __init__(self, locations: List[dict]):
        if not locations:
            self.message = "No location found. Please try another search."
        else:
            titles = [l['title'] for l in locations]
            self.message = "Multiple locations found (%s : %s). Please try and narrow down your search." \
                           % (len(locations), ', '.join(titles))


def today_is_rainy(keyword: str, search_location: Callable, fetch_forecast: Callable) -> bool:
    locations = search_location(keyword)

    if len(locations) != 1:
        raise LocationSearchFailure(locations)

    forecast = fetch_forecast(locations[0]['woeid'])
    todays_forecast = forecast['consolidated_weather'][0]
    return todays_forecast['weather_state_abbr'] in ('hr', 'lr', 's')
