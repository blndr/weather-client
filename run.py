import sys

from client.connector import search_location, fetch_forecast
from client.rain import today_is_rainy, LocationSearchFailure

keyword = sys.argv[1]

try:
    rainy = today_is_rainy(keyword, search_location=search_location, fetch_forecast=fetch_forecast)
except LocationSearchFailure as e:
    print(e.message)
else:
    if rainy:
        print('It will rain today, take your umbrella with you !')
    else:
        print('No rain today, leave the umbrella at home :-)')
