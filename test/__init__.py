heavy_rain_forecast = {
    "title": "Marseille",
    "location_type": "City",
    "woeid": 610264,
    "consolidated_weather": [
        {"weather_state_name": "Heavy Rain", "weather_state_abbr": "hr", "predictability": 90},
        {"weather_state_name": "Light Cloud", "weather_state_abbr": "lc", "predictability": 73},
        {"weather_state_name": "Heavy Cloud", "weather_state_abbr": "hc", "predictability": 56},
        {"weather_state_name": "Clear", "weather_state_abbr": "c", "predictability": 88},
        {"weather_state_name": "Hail", "weather_state_abbr": "h", "predictability": 23}
    ]
}

light_rain_forecast = {
    "title": "Marseille",
    "location_type": "City",
    "woeid": 610264,
    "consolidated_weather": [
        {"weather_state_name": "Light Rain", "weather_state_abbr": "lr", "predictability": 71},
        {"weather_state_name": "Light Cloud", "weather_state_abbr": "lc", "predictability": 73},
        {"weather_state_name": "Heavy Cloud", "weather_state_abbr": "hc", "predictability": 56},
        {"weather_state_name": "Clear", "weather_state_abbr": "c", "predictability": 88},
        {"weather_state_name": "Hail", "weather_state_abbr": "h", "predictability": 23}
    ]
}

showers_forecast = {
    "title": "Marseille",
    "location_type": "City",
    "woeid": 610264,
    "consolidated_weather": [
        {"weather_state_name": "Showers", "weather_state_abbr": "s", "predictability": 71},
        {"weather_state_name": "Light Cloud", "weather_state_abbr": "lc", "predictability": 73},
        {"weather_state_name": "Heavy Cloud", "weather_state_abbr": "hc", "predictability": 56},
        {"weather_state_name": "Clear", "weather_state_abbr": "c", "predictability": 88},
        {"weather_state_name": "Hail", "weather_state_abbr": "h", "predictability": 23}
    ]
}

clear_forecast = {
    "title": "Marseille",
    "location_type": "City",
    "woeid": 610264,
    "consolidated_weather": [
        {"weather_state_name": "Clear", "weather_state_abbr": "c", "predictability": 71},
        {"weather_state_name": "Light Cloud", "weather_state_abbr": "lc", "predictability": 73},
        {"weather_state_name": "Heavy Cloud", "weather_state_abbr": "hc", "predictability": 56},
        {"weather_state_name": "Clear", "weather_state_abbr": "c", "predictability": 88},
        {"weather_state_name": "Hail", "weather_state_abbr": "h", "predictability": 23}
    ]
}
