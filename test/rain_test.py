from unittest.mock import Mock

import pytest

from client.rain import today_is_rainy, LocationSearchFailure
from test import heavy_rain_forecast, light_rain_forecast, showers_forecast, clear_forecast


class TodayIsRainyTest:
    class LocationSearchFailureTest:
        def test_raises_an_exception_if_no_location_is_found(self):
            # given
            fetch_forecast = Mock()
            search_location = Mock()
            search_location.return_value = []

            # when
            with pytest.raises(LocationSearchFailure) as e:
                today_is_rainy('Caprica', search_location, fetch_forecast)

            # then
            assert e.value.message == "No location found. Please try another search."

        def test_raises_an_exception_if_several_location_matches_search(self):
            # given
            fetch_forecast = Mock()
            search_location = Mock()
            search_location.return_value = [
                {"title": "Tokyo", "location_type": "City", "woeid": 1118370, "latt_long": "35.670479,139.740921"},
                {"title": "Kyoto", "location_type": "City", "woeid": 15015372, "latt_long": "35.098129,135.718933"}
            ]

            # when
            with pytest.raises(LocationSearchFailure) as e:
                today_is_rainy('Kyo', search_location, fetch_forecast)

            # then
            assert e.value.message == "Multiple locations found (2 : Tokyo, Kyoto). Please try and narrow down your search."

    class RainyForecastTest:
        def test_returns_true_if_todays_forecast_is_heavy_rain(self):
            search_location = Mock()
            search_location.return_value = [
                {"title": "Marseille", "location_type": "City", "woeid": 610264, "latt_long": "43.293701,5.372470"}
            ]
            fetch_forecast = Mock()
            fetch_forecast.return_value = heavy_rain_forecast

            # when
            rainy = today_is_rainy('Marseille', search_location, fetch_forecast)

            assert rainy is True

        def test_returns_true_if_todays_forecast_is_light_rain(self):
            search_location = Mock()
            search_location.return_value = [
                {"title": "Marseille", "location_type": "City", "woeid": 610264, "latt_long": "43.293701,5.372470"}
            ]
            fetch_forecast = Mock()
            fetch_forecast.return_value = light_rain_forecast

            # when
            rainy = today_is_rainy('Marseille', search_location, fetch_forecast)

            assert rainy is True

        def test_returns_true_if_todays_forecast_is_showers(self):
            search_location = Mock()
            search_location.return_value = [
                {"title": "Marseille", "location_type": "City", "woeid": 610264, "latt_long": "43.293701,5.372470"}
            ]
            fetch_forecast = Mock()
            fetch_forecast.return_value = showers_forecast

            # when
            rainy = today_is_rainy('Marseille', search_location, fetch_forecast)

            assert rainy is True

    class NotRainyForecastTest:
        def test_returns_true_if_todays_forecast_is_not_rainy(self):
            search_location = Mock()
            search_location.return_value = [
                {"title": "Marseille", "location_type": "City", "woeid": 610264, "latt_long": "43.293701,5.372470"}
            ]
            fetch_forecast = Mock()
            fetch_forecast.return_value = clear_forecast

            # when
            rainy = today_is_rainy('Marseille', search_location, fetch_forecast)

            assert rainy is False
